package pl.edu.pwsztar.chess.domain;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * source na destination w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(Point source, Point destination);

    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if(source.getX() == destination.getX() && source.getY() == destination.getY()) {
                return false;
            }

            return Math.abs(destination.getX() - source.getX()) ==
                    Math.abs(destination.getY() - source.getY());
        }
    }

    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            return ((Math.abs(destination.getX() - source.getX()) == 2 && Math.abs(destination.getY() - source.getY()) == 1) ||
                    (Math.abs(destination.getX() - source.getX()) == 1 && Math.abs(destination.getY() - source.getY()) == 2));
        }
    }

    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            return (destination.getX() == source.getX() + 1 || destination.getX() == source.getX() - 1 || destination.getX() == source.getX()) &&
                    (destination.getY() == source.getY() + 1 || destination.getY() == source.getY() - 1 || destination.getY() == source.getY());
        }
    }

    class Queen implements RulesOfGame {
        @Override

        public boolean isCorrectMove(Point source, Point destination) {
            return ((Math.abs(destination.getX() - source.getX()) == Math.abs(destination.getY() - source.getY())) ||
                    (source.getX() == destination.getX() || source.getY() == destination.getY()));
        }
    }

    class Rook implements RulesOfGame {
        @Override

        public boolean isCorrectMove(Point source, Point destination) {
            return (destination.getX() == source.getX()) || (destination.getY() == source.getY());
        }
    }

    class Pawn implements RulesOfGame {
        @Override

        public boolean isCorrectMove(Point source, Point destination) {
            if (source.getY() == 6 && (source.getY() - 2 == destination.getY())) {
                return true;
            }
            return (destination.getY() == (source.getY()) - 1) && (destination.getX() == source.getX());
        }
    }
}
