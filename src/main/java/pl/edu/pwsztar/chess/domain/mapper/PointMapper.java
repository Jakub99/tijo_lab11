package pl.edu.pwsztar.chess.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.chess.domain.Point;

@Component
public class PointMapper {
    public Point toPoint2D(String position) {
        return new Point(Character.getNumericValue(position.charAt(0)) - 10,
                Math.abs(Character.getNumericValue(position.charAt(2))-8));
    }
}
