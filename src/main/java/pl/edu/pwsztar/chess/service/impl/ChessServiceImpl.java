package pl.edu.pwsztar.chess.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwsztar.chess.domain.Point;
import pl.edu.pwsztar.chess.domain.RulesOfGame;
import pl.edu.pwsztar.chess.domain.mapper.PointMapper;
import pl.edu.pwsztar.chess.dto.FigureMoveDto;
import pl.edu.pwsztar.chess.service.ChessService;

@Service
@Transactional
public class ChessServiceImpl implements ChessService {
    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame king;
    private RulesOfGame queen;
    private RulesOfGame rook;
    private RulesOfGame pawn;
    private PointMapper pointMapper;

    public ChessServiceImpl() {
        bishop = new RulesOfGame.Bishop();
        knight = new RulesOfGame.Knight();
        king = new RulesOfGame.King();
        queen = new RulesOfGame.Queen();
        rook = new RulesOfGame.Rook();
        pawn = new RulesOfGame.Pawn();

        pointMapper = new PointMapper();
    }

    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        switch (figureMoveDto.getType()) {
            case BISHOP:
                return bishop.isCorrectMove(pointMapper.toPoint2D(figureMoveDto.getSource()), pointMapper.toPoint2D(figureMoveDto.getDestination()));
            case KNIGHT:
                return knight.isCorrectMove(pointMapper.toPoint2D(figureMoveDto.getSource()), pointMapper.toPoint2D(figureMoveDto.getDestination()));
            case KING:
                return king.isCorrectMove(pointMapper.toPoint2D(figureMoveDto.getSource()), pointMapper.toPoint2D(figureMoveDto.getDestination()));
            case QUEEN:
                return queen.isCorrectMove(pointMapper.toPoint2D(figureMoveDto.getSource()), pointMapper.toPoint2D(figureMoveDto.getDestination()));
            case ROOK:
                return rook.isCorrectMove(pointMapper.toPoint2D(figureMoveDto.getSource()), pointMapper.toPoint2D(figureMoveDto.getDestination()));
            case PAWN:
                return pawn.isCorrectMove(pointMapper.toPoint2D(figureMoveDto.getSource()), pointMapper.toPoint2D(figureMoveDto.getDestination()));
        }

        return false;
    }
}
